package main

import (
	"bufio"
	"fmt"
	"image"
	"image/draw"
	"log"
	"os"
	"strings"

	"github.com/disintegration/imaging"
	"github.com/fogleman/gg"
)

type Request struct {
	FontPath   string
	BackGround string
	FontSize   float64
}

func main() {

	var username string
	fmt.Print("Enter the username: ")
	fmt.Scan(&username)

	var ImgPath string = username + "/dp.jpeg"

	file1, _ := os.Open(username + "/name.txt")
	f1 := bufio.NewReader(file1)

	var strArray1 [2]string

	for i := 0; i < 2; i++ {
		strArray1[i], _ = f1.ReadString(' ')
	}

	Fname := strArray1[0]
	Lname := strArray1[1]

	imgFile1, err := os.Open("bg.png")
	if err != nil {
		fmt.Println(err)
	}
	imgFile2, err := os.Open(ImgPath)
	if err != nil {
		fmt.Println(err)
	}
	img1, _, err := image.Decode(imgFile1)
	if err != nil {
		fmt.Println(err)
	}
	img, _, err := image.Decode(imgFile2)
	if err != nil {
		fmt.Println(err)
	}
	img2 := imaging.Resize(img, 480, 480, imaging.Lanczos)

	sp2 := image.Point{img1.Bounds().Dx() - img2.Bounds().Dx(), 0}
	r2 := image.Rectangle{sp2, sp2.Add(img1.Bounds().Size())}
	rgba := image.NewRGBA(image.Rectangle{image.Point{0, 0}, r2.Size()})

	draw.Draw(rgba, img1.Bounds(), img1, image.Point{0, 0}, draw.Src)
	draw.Draw(rgba, r2, img2, image.Point{0, 0}, draw.Src)

	dc := gg.NewContext(1196, 495)

	dc.DrawImage(rgba, 0, 0)

	if err := dc.LoadFontFace("arial.ttf", 50); err != nil {
		panic(err)
	}
	dc.SetHexColor("#000")
	dc.DrawStringAnchored(Fname, 440, 85, 0, 0)
	dc.DrawStringAnchored(Lname, 440, 140, 0, 0)

	dc.SavePNG(username + "out.png")

	desc := Request{
		FontPath:   "arial.ttf",
		BackGround: username + "out.png",
		FontSize:   20,
	}

	back, err := gg.LoadImage(desc.BackGround)
	if err != nil {
		panic(err)
	}

	file, _ := os.Open(username + "/text.txt")
	f := bufio.NewReader(file)

	var strArray [7]string

	for i := 0; i < len(strArray); i++ {
		strArray[i], _ = f.ReadString('\n')
		strArray[i] = strings.TrimRight(strArray[i], "\r\n")
	}

	dc1 := gg.NewContext(1200, 628)

	dc1.DrawImage(back, 0, 0)

	if err := dc1.LoadFontFace(desc.FontPath, desc.FontSize); err != nil {
		panic(err)
	}

	dc1.SetHexColor("#808080")

	y := 300.0
	for i := 0; i < len(strArray); i++ {
		dc1.DrawStringAnchored(strArray[i], 52, y, 0, 0)
		y = y + 20.0
	}

	dc1.SavePNG(username + "out.png")

	link := Request{
		FontPath:   "arial.ttf",
		BackGround: username + "out.png",
		FontSize:   25,
	}

	comp, err := gg.LoadImage(link.BackGround)
	if err != nil {
		panic(err)
	}

	dc2 := gg.NewContext(1196, 495)

	dc2.DrawImage(comp, 0, 0)

	if err := dc2.LoadFontFace(link.FontPath, link.FontSize); err != nil {
		panic(err)
	}

	dc2.SetHexColor("#808080")

	dc2.DrawStringAnchored("BIO.CONVOSIGHT.COM", 15, 530, 0, 0)
	dc2.SetHexColor("#000")
	dc2.DrawStringAnchored(Fname+Lname+"'s admin bio on Convosight."+Fname, 15, 570, 0, 0)
	dc2.DrawStringAnchored(Lname+"'s key achievement, community contribution. S...", 15, 610, 0, 0)
	log.Println(dc2)

	dc2.SavePNG(username + "out.png")
}
